//
//  main.cpp
//  OpenGL Shadows
//
//  Created by CGIS on 05/12/16.
//  Copyright � 2016 CGIS. All rights reserved.
//

#define GLEW_STATIC

#include <iostream>
#include "glm/glm.hpp"//core glm functionality
#include "glm/gtc/matrix_transform.hpp"//glm extension for generating common transformation matrices
#include "glm/gtc/matrix_inverse.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "GLEW/glew.h"
#include "GLFW/glfw3.h"
#include <string>
#include "Shader.hpp"
#include "Camera.hpp"
#include "SkyBox.hpp"
#include <vector>
#define TINYOBJLOADER_IMPLEMENTATION

#include "Model3D.hpp"
#include "Mesh.hpp"

int glWindowWidth = 1366;
int glWindowHeight = 768;
int retina_width, retina_height;
GLFWwindow* glWindow = NULL;

const GLuint SHADOW_WIDTH = 2048, SHADOW_HEIGHT = 2048;

glm::mat4 model;
GLuint modelLoc;
glm::mat4 view;
GLuint viewLoc;
glm::mat4 projection;
GLuint projectionLoc;
glm::mat3 normalMatrix;
GLuint normalMatrixLoc;
glm::mat3 lightDirMatrix;
GLuint lightDirMatrixLoc;

glm::vec3 lightDir;
GLuint lightDirLoc;
glm::vec3 lightColor;
GLuint lightColorLoc;
glm::vec3 PointLightPos[22];
GLuint PointLightPosLoc[22];

gps::Camera myCamera(glm::vec3(4.0f, 5.0f, 10.0f), glm::vec3(0.0f, 0.0f, 0.0f));
GLfloat cameraSpeed = 0.08f;

bool pressedKeys[1024];
GLfloat lightAngle;

bool turnOnOffLight = true;
GLuint switchDLoc;
bool turnOnOffPointLight = false;
GLuint switchPLoc;
bool turnOnOffFog = false;
GLuint fogLoc;


gps::Model3D ground;
gps::Model3D soldier1;
gps::Model3D bec;
gps::Model3D ground1;
gps::Model3D house1;
gps::Model3D lightCube;
gps::Model3D soldier2;

gps::SkyBox mySkyBox;
gps::SkyBox mySkyBox2;


gps::Shader myCustomShader;
gps::Shader lightShader;
gps::Shader depthMapShader;
gps::Shader skyboxShader;
GLuint shadowMapFBO;
GLuint depthMapTexture;

GLenum glCheckError_(const char *file, int line)
{
	GLenum errorCode;
	while ((errorCode = glGetError()) != GL_NO_ERROR)
	{
		std::string error;
		switch (errorCode)
		{
		case GL_INVALID_ENUM:                  error = "INVALID_ENUM"; break;
		case GL_INVALID_VALUE:                 error = "INVALID_VALUE"; break;
		case GL_INVALID_OPERATION:             error = "INVALID_OPERATION"; break;
		case GL_STACK_OVERFLOW:                error = "STACK_OVERFLOW"; break;
		case GL_STACK_UNDERFLOW:               error = "STACK_UNDERFLOW"; break;
		case GL_OUT_OF_MEMORY:                 error = "OUT_OF_MEMORY"; break;
		case GL_INVALID_FRAMEBUFFER_OPERATION: error = "INVALID_FRAMEBUFFER_OPERATION"; break;
		}
		std::cout << error << " | " << file << " (" << line << ")" << std::endl;
	}
	return errorCode;
}
#define glCheckError() glCheckError_(__FILE__, __LINE__)

//aici au loc initializarea shaderelor si a matricii projection
void windowResizeCallback(GLFWwindow* window, int width, int height)
{
	fprintf(stdout, "window resized to width: %d , and height: %d\n", width, height);
	//TODO
	//for RETINA display
	glfwGetFramebufferSize(glWindow, &retina_width, &retina_height);

	myCustomShader.useShaderProgram();

	//set projection matrix
	glm::mat4 projection = glm::perspective(glm::radians(45.0f), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);
	//send matrix data to shader
	GLint projLoc = glGetUniformLocation(myCustomShader.shaderProgram, "projection");
	glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));

	lightShader.useShaderProgram();

	glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

	//set Viewport transform
	glViewport(0, 0, retina_width, retina_height);
}

void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
			pressedKeys[key] = true;
		else if (action == GLFW_RELEASE)
			pressedKeys[key] = false;
	}
}


GLfloat yaw = -90.0f;
GLfloat pitch = 0.0f;
GLfloat roll = 0.0f;
GLfloat lastX = 600, lastY = 400;	//jumatatea dimensiunilor
void mouseCallback(GLFWwindow* window, double xpos, double ypos)
{
	GLfloat xoffset = xpos - lastX;
	GLfloat yoffset = lastY - ypos; // Reversed since y-coordinates range from bottom to top
	lastX = xpos;
	lastY = ypos;

	GLfloat sensitivity = 0.05f;
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	yaw += xoffset;
	pitch += yoffset;

	myCamera.rotate(pitch, yaw);
}


int displayMode;
void displayModes()
{
	switch (displayMode)
	{
	case 1: glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		break;
	case 2: glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		break;
	case 3: glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
		break;
	default:
		break;
	}

}

GLfloat forward = 0.0;
void processMovement()
{
	if (pressedKeys[GLFW_KEY_UP]) {
		forward += 0.04f;
	}
	if (pressedKeys[GLFW_KEY_DOWN]) {
		forward -= 0.04f;
	}

	if (pressedKeys[GLFW_KEY_1]) {
		displayMode = 1;
	}
	if (pressedKeys[GLFW_KEY_2]) {
		displayMode = 2;
	}
	if (pressedKeys[GLFW_KEY_3]) {
		displayMode = 3;
	}

	if (pressedKeys[GLFW_KEY_W]) {
		myCamera.move(gps::MOVE_FORWARD, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_S]) {
		myCamera.move(gps::MOVE_BACKWARD, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_A]) {
		myCamera.move(gps::MOVE_LEFT, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_D]) {
		myCamera.move(gps::MOVE_RIGHT, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_J]) {

		lightAngle += 0.3f;
		if (lightAngle > 360.0f)
			lightAngle -= 360.0f;
		glm::vec3 lightDirTr = glm::vec3(glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(lightDir, 1.0f));
		myCustomShader.useShaderProgram();
		glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDirTr));
	}

	if (pressedKeys[GLFW_KEY_L]) {
		lightAngle -= 0.3f;
		if (lightAngle < 0.0f)
			lightAngle += 360.0f;
		glm::vec3 lightDirTr = glm::vec3(glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(lightDir, 1.0f));
		myCustomShader.useShaderProgram();
		glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDirTr));
	}

	if (pressedKeys[GLFW_KEY_X]) {
		turnOnOffLight = !turnOnOffLight;
	}
	myCustomShader.useShaderProgram();
	switchDLoc = glGetUniformLocation(myCustomShader.shaderProgram, "turnOnOffLight");
	glUniform1i(switchDLoc, turnOnOffLight);

	if (pressedKeys[GLFW_KEY_Z]) {
		turnOnOffPointLight = !turnOnOffPointLight;
	}
	myCustomShader.useShaderProgram();
	switchPLoc = glGetUniformLocation(myCustomShader.shaderProgram, "turnOnOffPointLight");
	glUniform1i(switchPLoc, turnOnOffPointLight);


	if (pressedKeys[GLFW_KEY_F]) {
		turnOnOffFog = !turnOnOffFog;
	}
	myCustomShader.useShaderProgram();
	fogLoc = glGetUniformLocation(myCustomShader.shaderProgram, "turnOnOffFog");
	glUniform1i(fogLoc, turnOnOffFog);
}

bool initOpenGLWindow()
{
	if (!glfwInit()) {
		fprintf(stderr, "ERROR: could not start GLFW3\n");
		return false;
	}

	//for Mac OS X
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glWindow = glfwCreateWindow(glWindowWidth, glWindowHeight, "OpenGL Shader Example", NULL, NULL);
	if (!glWindow) {
		fprintf(stderr, "ERROR: could not open window with GLFW3\n");
		glfwTerminate();
		return false;
	}

	glfwSetWindowSizeCallback(glWindow, windowResizeCallback);
	glfwMakeContextCurrent(glWindow);

	glfwWindowHint(GLFW_SAMPLES, 4);

	// start GLEW extension handler
	glewExperimental = GL_TRUE;
	glewInit();

	// get version info
	const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
	const GLubyte* version = glGetString(GL_VERSION); // version as a string
	printf("Renderer: %s\n", renderer);
	printf("OpenGL version supported %s\n", version);

	//for RETINA display
	glfwGetFramebufferSize(glWindow, &retina_width, &retina_height);

	glfwSetKeyCallback(glWindow, keyboardCallback);
	glfwSetCursorPosCallback(glWindow, mouseCallback);
	glfwSetInputMode(glWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	return true;
}

void initOpenGLState()
{
	glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
	glViewport(0, 0, retina_width, retina_height);

	glEnable(GL_DEPTH_TEST); // enable depth-testing
	glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"
	glEnable(GL_CULL_FACE); // cull face
	glCullFace(GL_BACK); // cull back face
	glFrontFace(GL_CCW); // GL_CCW for counter clock-wise
}

void initFBOs()
{
	//generate FBO ID
	glGenFramebuffers(1, &shadowMapFBO);

	//create depth texture for FBO
	glGenTextures(1, &depthMapTexture);
	glBindTexture(GL_TEXTURE_2D, depthMapTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
		SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	//attach texture to FBO
	glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMapTexture, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

glm::mat4 computeLightSpaceTrMatrix()
{
	const GLfloat near_plane = -20.0f, far_plane = 40.0f;
	glm::mat4 lightProjection = glm::ortho(-30.0f, 30.0f, -30.0f, 30.0f, near_plane, far_plane);

	glm::vec3 lightDirTr = glm::vec3(glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(lightDir, 1.0f));
	glm::mat4 lightView = glm::lookAt(lightDirTr, myCamera.getCameraTarget(), glm::vec3(0.0f, 1.0f, 0.0f));

	return lightProjection * lightView;
}

void initModels()
{
	ground = gps::Model3D("objects/ground/ground.obj", "objects/ground/");
	soldier1 = gps::Model3D("objects/soldier1/soldier1.obj", "objects/soldier1/");
	soldier2 = gps::Model3D("objects/soldier1/soldier2.obj", "objects/soldier1/");
	ground1 = gps::Model3D("objects/ground/ground1.obj", "objects/ground1/");
	//sandStreet = gps::Model3D("objects/ground/sandStreet.obj", "objects/ground/");
	//street = gps::Model3D("objects/ground/street.obj", "objects/ground/");
	//myModelPietre = gps::Model3D("objects/Bolovani/Bolovani.obj", "objects/Bolovani/");
	//myModelCort = gps::Model3D("objects/Tent_1/Tent_1.obj", "objects/Tent_1/");
	lightCube = gps::Model3D("objects/cube/cube.obj", "objects/cube/");
	house1 = gps::Model3D("objects/house2/house2.obj", "objects/house2/");
	bec = gps::Model3D("objects/bec/bec.obj", "objects/bec/");
	//jeep = gps::Model3D("objects/Jeep/Jeep.obj", "objects/Jeep/");
	//bambooHouse = gps::Model3D("objects/bambooHouse/bambooHouse.obj", "objects/bambooHouse/");
	//littleHouse = gps::Model3D("objects/littleHouse/littleHouse.obj", "objects/littleHouse/");
}

void initShaders()
{
	myCustomShader.loadShader("shaders/myShader.vert", "shaders/myShader.frag");
	lightShader.loadShader("shaders/lightCube.vert", "shaders/lightCube.frag");
	depthMapShader.loadShader("shaders/simpleDepthMap.vert", "shaders/simpleDepthMap.frag");

	std::vector<const GLchar*> faces;
	faces.push_back("objects/ame_desert/desertsky_rt.tga");
	faces.push_back("objects/ame_desert/desertsky_lf.tga");
	faces.push_back("objects/ame_desert/desertsky_up.tga");
	faces.push_back("objects/ame_desert/desertsky_dn.tga");
	faces.push_back("objects/ame_desert/desertsky_bk.tga");
	faces.push_back("objects/ame_desert/desertsky_ft.tga");
	mySkyBox.Load(faces);

	/*std::vector<const GLchar*> faces2;
	faces2.push_back("objects/ame_nebula/purplenebula_rt.tga");
	faces2.push_back("objects/ame_nebula/purplenebula_lf.tga");
	faces2.push_back("objects/ame_nebula/purplenebula_up.tga");
	faces2.push_back("objects/ame_nebula/purplenebula_dn.tga");
	faces2.push_back("objects/ame_nebula/purplenebula_bk.tga");
	faces2.push_back("objects/ame_nebula/purplenebula_ft.tga");
	mySkyBox2.Load(faces2);
*/
	skyboxShader.loadShader("shaders/skyboxShader.vert", "shaders/skyboxShader.frag");
	skyboxShader.useShaderProgram();
	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "view"), 1, GL_FALSE,
		glm::value_ptr(view));

	projection = glm::perspective(glm::radians(45.0f), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);
	glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "projection"), 1, GL_FALSE,
		glm::value_ptr(projection));

}



int i, j;
int index1, index2;
void initUniforms()
{
	myCustomShader.useShaderProgram();

	modelLoc = glGetUniformLocation(myCustomShader.shaderProgram, "model");

	viewLoc = glGetUniformLocation(myCustomShader.shaderProgram, "view");

	normalMatrixLoc = glGetUniformLocation(myCustomShader.shaderProgram, "normalMatrix");

	lightDirMatrixLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightDirMatrix");

	projection = glm::perspective(glm::radians(45.0f), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);
	projectionLoc = glGetUniformLocation(myCustomShader.shaderProgram, "projection");
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

	//set the light direction (direction towards the light)
	lightDir = glm::vec3(0.0f, 3.0f, -5.0f);
	lightDirLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightDir");
	glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDir));

	PointLightPos[0] = glm::vec3(-1.0f, 0.5f, -10.0f);
	PointLightPosLoc[0] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[0]");
	glUniform3fv(PointLightPosLoc[0], 1, glm::value_ptr(PointLightPos[0]));
	PointLightPos[1] = glm::vec3(1.0f, 0.5f, -10.0f);
	PointLightPosLoc[1] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[1]");
	glUniform3fv(PointLightPosLoc[1], 1, glm::value_ptr(PointLightPos[1]));

	PointLightPos[2] = glm::vec3(-1.0f, 0.5f, -8.0f);
	PointLightPosLoc[2] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[2]");
	glUniform3fv(PointLightPosLoc[2], 1, glm::value_ptr(PointLightPos[2]));
	PointLightPos[3] = glm::vec3(1.0f, 0.5f, -8.0f);
	PointLightPosLoc[3] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[3]");
	glUniform3fv(PointLightPosLoc[3], 1, glm::value_ptr(PointLightPos[3]));

	PointLightPos[4] = glm::vec3(-1.0f, 0.5f, -6.0f);
	PointLightPosLoc[4] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[4]");
	glUniform3fv(PointLightPosLoc[4], 1, glm::value_ptr(PointLightPos[4]));
	PointLightPos[5] = glm::vec3(1.0f, 0.5f, -6.0f);
	PointLightPosLoc[5] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[5]");
	glUniform3fv(PointLightPosLoc[5], 1, glm::value_ptr(PointLightPos[5]));

	PointLightPos[6] = glm::vec3(-1.0f, 0.5f, -4.0f);
	PointLightPosLoc[6] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[6]");
	glUniform3fv(PointLightPosLoc[6], 1, glm::value_ptr(PointLightPos[6]));
	PointLightPos[7] = glm::vec3(1.0f, 0.5f, -4.0f);
	PointLightPosLoc[7] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[7]");
	glUniform3fv(PointLightPosLoc[7], 1, glm::value_ptr(PointLightPos[7]));

	PointLightPos[8] = glm::vec3(-1.0f, 0.5f, -2.0f);
	PointLightPosLoc[8] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[8]");
	glUniform3fv(PointLightPosLoc[8], 1, glm::value_ptr(PointLightPos[8]));
	PointLightPos[9] = glm::vec3(1.0f, 0.5f, -2.0f);
	PointLightPosLoc[9] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[9]");
	glUniform3fv(PointLightPosLoc[9], 1, glm::value_ptr(PointLightPos[9]));

	PointLightPos[10] = glm::vec3(-1.0f, 0.5f, 0.0f);
	PointLightPosLoc[10] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[10]");
	glUniform3fv(PointLightPosLoc[10], 1, glm::value_ptr(PointLightPos[10]));
	PointLightPos[11] = glm::vec3(1.0f, 0.5f, 0.0f);
	PointLightPosLoc[11] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[11]");
	glUniform3fv(PointLightPosLoc[11], 1, glm::value_ptr(PointLightPos[11]));


	PointLightPos[12] = glm::vec3(-1.0f, 0.5f, 2.0f);
	PointLightPosLoc[12] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[12]");
	glUniform3fv(PointLightPosLoc[12], 1, glm::value_ptr(PointLightPos[12]));
	PointLightPos[13] = glm::vec3(1.0f, 0.5f, 2.0f);
	PointLightPosLoc[13] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[13]");
	glUniform3fv(PointLightPosLoc[13], 1, glm::value_ptr(PointLightPos[13]));


	PointLightPos[14] = glm::vec3(-1.0f, 0.5f, 4.0f);
	PointLightPosLoc[14] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[14]");
	glUniform3fv(PointLightPosLoc[14], 1, glm::value_ptr(PointLightPos[14]));
	PointLightPos[15] = glm::vec3(1.0f, 0.5f, 4.0f);
	PointLightPosLoc[15] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[15]");
	glUniform3fv(PointLightPosLoc[15], 1, glm::value_ptr(PointLightPos[15]));


	PointLightPos[16] = glm::vec3(-1.0f, 0.5f, 6.0f);
	PointLightPosLoc[16] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[16]");
	glUniform3fv(PointLightPosLoc[16], 1, glm::value_ptr(PointLightPos[16]));
	PointLightPos[17] = glm::vec3(1.0f, 0.5f, 6.0f);
	PointLightPosLoc[17] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[17]");
	glUniform3fv(PointLightPosLoc[17], 1, glm::value_ptr(PointLightPos[17]));

	PointLightPos[18] = glm::vec3(-1.0f, 0.5f, 8.0f);
	PointLightPosLoc[18] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[18]");
	glUniform3fv(PointLightPosLoc[18], 1, glm::value_ptr(PointLightPos[18]));
	PointLightPos[19] = glm::vec3(1.0f, 0.5f, 8.0f);
	PointLightPosLoc[19] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[19]");
	glUniform3fv(PointLightPosLoc[19], 1, glm::value_ptr(PointLightPos[19]));


	PointLightPos[20] = glm::vec3(-1.0f, 0.5f, 10.0f);
	PointLightPosLoc[20] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[20]");
	glUniform3fv(PointLightPosLoc[20], 1, glm::value_ptr(PointLightPos[20]));
	PointLightPos[21] = glm::vec3(1.0f, 0.5f, 10.0f);
	PointLightPosLoc[21] = glGetUniformLocation(myCustomShader.shaderProgram, "posOfPointLight[21]");
	glUniform3fv(PointLightPosLoc[21], 1, glm::value_ptr(PointLightPos[21]));

	//set light color
	lightColor = glm::vec3(1.0f, 1.0f, 1.0f); //white light
	lightColorLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightColor");
	glUniform3fv(lightColorLoc, 1, glm::value_ptr(lightColor));

	lightShader.useShaderProgram();
	glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

}


void renderScene()
{
	displayModes();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (turnOnOffLight)
		mySkyBox.Draw(skyboxShader, view, projection);
	else
		mySkyBox2.Draw(skyboxShader, view, projection);

	processMovement();

	//render the scene to the depth buffer

	depthMapShader.useShaderProgram();
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "lightSpaceTrMatrix"), 1, GL_FALSE, glm::value_ptr(computeLightSpaceTrMatrix()));
	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);

	//FBO_ground
	//create model matrix for ground
	index1 = -10, index2 = -10;
	while (index1 <= 10)
	{
		index2 = -10;
		while (index2 <= 10)
		{
			model = glm::translate(glm::mat4(1.0f), glm::vec3(index1, -0.2f, index2));
			model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
			//send model matrix to shader
			glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
			ground.Draw(depthMapShader);
			index2 += 1;
		}
		index1 += 1;
	}


	////FBO_strada
	//index1 = 0, index2 = -10;
	//while (index1 <= 1) {
	//	index2 = -10;
	//	while (index2 <= 10) {
	//		model = glm::translate(glm::mat4(1.0f), glm::vec3(index1, -0.198f, index2));
	//		model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
	//		glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	//		street.Draw(depthMapShader);
	//		index2 += 1;
	//	}
	//	index1 += 1;
	//}

	////FBO_bloc
	//model = glm::mat4(1.0f);
	//model = glm::translate(model, glm::vec3(0, -0.45f, 0));
	//model = glm::translate(model, glm::vec3(-2.0f, 0.0f, 8.0f));
	//model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
	//model = glm::scale(model, glm::vec3(1.0f, 1.0f, 1.0f));

	//glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	//house1.Draw(depthMapShader);

	////FBO_house
	//model = glm::mat4(1.0f);
	//model = glm::translate(model, glm::vec3(-1.0f, -0.2f, -12.5f));
	//model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	//model = glm::scale(model, glm::vec3(0.4f, 0.4f, 0.4f));

	//glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	//bambooHouse.Draw(depthMapShader);

	//FBO_lumini
	index1 = 0;
	while (index1 <= 20)
	{
		model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(-1.0f, -0.2f, -10.0f + index1));
		model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
		//send model matrix to shader
		glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
		bec.Draw(depthMapShader);
		model = glm::mat4(1.0f);
		model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::translate(model, glm::vec3(1.0f, -0.2f, -10.0f + index1));
		model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
		glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
		bec.Draw(depthMapShader);
		index1 += 2;

	}

	////FBO_JEEP (pe nisip)
	//for (int i = 0; i < 5; i++) {
	//	model = glm::mat4(1.0f);
	//	model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 2.0f));
	//	model = glm::translate(model, glm::vec3(0.0f, -0.2f, 0.0f));
	//	model = glm::translate(model, glm::vec3(4.5f + 2 * i, 0.0f, 11.0f));
	//	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
	//	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 0, 1));
	//	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
	//	model = glm::scale(model, glm::vec3(0.008f, 0.008f, 0.008f));     //transformare 1
	//	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	//	jeep.Draw(depthMapShader);
	//}

	////FBO_JEEP (in parcare)
	//for (int i = 0; i < 5; i++) {
	//	model = glm::mat4(1.0f);
	//	model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0, 1, 0));
	//	model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 2.0f));
	//	model = glm::translate(model, glm::vec3(0.0f, -0.2f, 0.0f));
	//	model = glm::translate(model, glm::vec3(4.5f + 2 * i, 0.0f, 0.0f));
	//	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
	//	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 0, 1));
	//	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
	//	model = glm::scale(model, glm::vec3(0.008f, 0.008f, 0.008f));     //transformare 1
	//																	  //send model matrix data to shader	
	//	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	//	jeep.Draw(depthMapShader);
	//}

	////FBO_JEEP (on the street)
	//model = glm::mat4(1.0f);
	//model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0, 1, 0));
	//model = glm::translate(model, glm::vec3(0.0f, -0.2f, 0.0f));
	//if (10.0f - forward < -6.6f) {
	//	forward = 16.6f;
	//}
	//else {
	//	if (10.0f - forward > 14.0f)
	//		forward = -4.0f;
	//}
	//model = glm::translate(model, glm::vec3(1.6f, 0.0f, 10.0f - forward));
	//model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
	//model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 0, 1));
	//model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
	//model = glm::scale(model, glm::vec3(0.008f, 0.008f, 0.008f));
	//glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	//jeep.Draw(depthMapShader);


	////FBO_littleHouse
	//model = glm::mat4(1.0f);
	//model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
	//model = glm::translate(model, glm::vec3(17.0f, -0.9f, -17.5f));

	//glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	//littleHouse.Draw(depthMapShader);

	////FBO_pietre
	//for (int i = 0; i < 5; i++) {
	//	model = glm::mat4(1.0f);
	//	model = glm::translate(model, glm::vec3(9.6f - 2 * i, 0.0f, -10.0f));
	//	model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	//	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	//	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	//	myModelPietre.Draw(depthMapShader);
	//}

	////FBO_corturi
	//for (int k = 0; k < 3; k++) {
	//	for (int i = 0; i < 3; i++) {
	//		model = glm::mat4(1.0f);
	//		model = glm::translate(model, glm::vec3(10.0f - 2 * i, -0.2f, -3.0f - 2 * k));
	//		model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	//		model = glm::scale(model, glm::vec3(0.08f, 0.08f, 0.08f));
	//		glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	//		myModelCort.Draw(depthMapShader);
	//	}
	//}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	//Render the scene from the camera point of view

	myCustomShader.useShaderProgram();

	//send lightSpace matrix to shader
	glUniformMatrix4fv(glGetUniformLocation(myCustomShader.shaderProgram, "lightSpaceTrMatrix"), 1, GL_FALSE, glm::value_ptr(computeLightSpaceTrMatrix()));

	//send view matrix to shader
	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(glGetUniformLocation(myCustomShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));

	//compute light direction transformation matrix
	lightDirMatrix = glm::mat3(glm::inverseTranspose(view));

	//send lightDir matrix data to shader
	glUniformMatrix3fv(lightDirMatrixLoc, 1, GL_FALSE, glm::value_ptr(lightDirMatrix));
	glViewport(0, 0, retina_width, retina_height);
	myCustomShader.useShaderProgram();

	//bind the depth map
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, depthMapTexture);
	glUniform1i(glGetUniformLocation(myCustomShader.shaderProgram, "shadowMap"), 3);


	//Ground
	index1 = -10; index2 = -10;
	while (index1 <= 10)
	{
		index2 = -10;
		while (index2 <= 10)
		{
			model = glm::translate(glm::mat4(1.0f), glm::vec3(index1, -0.2f, index2));
			model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
			//send model matrix to shader
			glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
			//create normal matrix
			normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
			//send normal matrix data to shader
			glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
			ground.Draw(myCustomShader);
			index2 += 2;
		}
		index1 += 2;
	}

	////Nisip
	//model = glm::mat4(1.0f);
	//model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	//model = glm::translate(model, glm::vec3(12.0f, -0.35f, 12.0f));
	////send model matrix to shader
	//glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	////create normal matrix
	//normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	////send normal matrix data to shader
	//glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	//sand.Draw(myCustomShader);

	//model = glm::mat4(1.0f);
	//model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	//model = glm::translate(model, glm::vec3(12.0f, -0.35f, -12.0f));
	////send model matrix to shader
	//glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	////create normal matrix
	//normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	////send normal matrix data to shader
	//glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	//sand.Draw(myCustomShader);

	////STRADA NISIP
	//for (int i = 0; i < 5; i++) {
	//	model = glm::mat4(1.0f);
	//	model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
	//	model = glm::translate(model, glm::vec3(20.0f + 20 * i, -1.8f, 0.0f));
	//	//send model matrix to shader
	//	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//	//create normal matrix
	//	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//	//send normal matrix data to shader
	//	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	//	sandStreet.Draw(myCustomShader);
	//}
	

	////LittleHouse
	//model = glm::mat4(1.0f);
	//model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
	//model = glm::translate(model, glm::vec3(17.0f, -0.9f, -17.5f));
	//glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	////create normal matrix
	//normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	////send normal matrix data to shader
	//glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	//littleHouse.Draw(myCustomShader);

	///Soldier1
	//normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	
	////Parcare blocuri
	//for (int k = 0; k < 3; k++) {
	//	for (int i = 0; i < 5; i++) {
	//		model = glm::mat4(1.0f);
	//		model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
	//		model = glm::translate(model, glm::vec3(-100.0f + 20 * i, -1.8f, -20.0f + 20 * k));
	//		//send model matrix to shader
	//		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//		//create normal matrix
	//		normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//		//send normal matrix data to shader
	//		glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	//		sandStreet.Draw(myCustomShader);
	//	}
	//}

	////Strada	
	//index1 = -0.5, index2 = -10;
	//while (index2 <= 10) {
	//	model = glm::translate(glm::mat4(1.0f), glm::vec3(index1, -0.198f, index2));
	//	model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
	//	//send model matrix to shader
	//	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//	//create normal matrix
	//	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//	//send normal matrix data to shader
	//	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	//	street.Draw(myCustomShader);
	//	index2 += 2;
	//}

	model = glm::mat4(1.0f);
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	soldier1.Draw(myCustomShader);


	model = glm::mat4(1.0f);
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	soldier2.Draw(myCustomShader);


	////Bloc		
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(0, -0.45f, 0));
	model = glm::translate(model, glm::vec3(-2.0f, 0.0f, 8.0f));
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
	model = glm::scale(model, glm::vec3(1.0f, 1.0f, 1.0f));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	house1.Draw(myCustomShader);

	//Lumini		
	index1 = 0;
	while (index1 <= 20)
	{
		model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(-1.0f, -0.2f, -10.0f + index1));
		model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));

		//send model matrix data to shader
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		//create normal matrix
		normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
		//send normal matrix data to shader
		glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
		bec.Draw(myCustomShader);

		model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(1.0f, -0.2f, -10.0f + index1));
		model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
		//send model matrix data to shader
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		//create normal matrix
		normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
		//send normal matrix data to shader
		glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
		bec.Draw(myCustomShader);
		index1 += 2;
	}

	////House
	//model = glm::mat4(1.0f);
	//model = glm::translate(model, glm::vec3(-1.0f, -0.2f, -12.5f));
	//model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	//model = glm::scale(model, glm::vec3(0.4f, 0.4f, 0.4f));
	////send model matrix data to shader
	//glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	////create normal matrix
	//normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	////send normal matrix data to shader
	//glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	//bambooHouse.Draw(myCustomShader);


	////JEEP (pe nisip)
	//for (int i = 0; i < 5; i++) {
	//	model = glm::mat4(1.0f);
	//	model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 2.0f));
	//	model = glm::translate(model, glm::vec3(0.0f, -0.2f, 0.0f));
	//	model = glm::translate(model, glm::vec3(4.5f + 2 * i, 0.0f, 11.0f));
	//	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
	//	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 0, 1));
	//	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
	//	model = glm::scale(model, glm::vec3(0.008f, 0.008f, 0.008f));     //transformare 1
	//																	  //send model matrix data to shader	
	//	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//	//compute normal matrix
	//	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//	//send normal matrix data to shader
	//	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	//	jeep.Draw(myCustomShader);
	//}

	////JEEP (in parcare)
	//for (int i = 0; i < 5; i++) {
	//	model = glm::mat4(1.0f);
	//	model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0, 1, 0));
	//	model = glm::translate(model, glm::vec3(-1.0f, 0.0f, 2.0f));
	//	model = glm::translate(model, glm::vec3(0.0f, -0.2f, 0.0f));
	//	model = glm::translate(model, glm::vec3(4.5f + 2 * i, 0.0f, 0.0f));
	//	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
	//	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 0, 1));
	//	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
	//	model = glm::scale(model, glm::vec3(0.008f, 0.008f, 0.008f));     //transformare 1
	//																	  //send model matrix data to shader	
	//	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//	//compute normal matrix
	//	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//	//send normal matrix data to shader
	//	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	//	jeep.Draw(myCustomShader);
	//}

	////JEEP (on the street)
	//model = glm::mat4(1.0f);
	//model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0, 1, 0));
	//model = glm::translate(model, glm::vec3(0.0f, -0.2f, 0.0f));
	//if (10.0f - forward < -6.6f) {
	//	forward = 16.6f;
	//}
	//else {
	//	if (10.0f - forward > 14.0f)
	//		forward = -4.0f;
	//}
	//model = glm::translate(model, glm::vec3(1.6f, 0.0f, 10.0f - forward));
	//model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
	//model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 0, 1));
	//model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
	//model = glm::scale(model, glm::vec3(0.008f, 0.008f, 0.008f));
	//glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	////compute normal matrix
	//normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	////send normal matrix data to shader
	//glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	//jeep.Draw(myCustomShader);

	////Stanci
	//for (int i = 0; i < 5; i++) {
	//	model = glm::mat4(1.0f);
	//	model = glm::translate(model, glm::vec3(9.6f - 2 * i, 0.0f, -10.0f));
	//	model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	//	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	//	//send model matrix data to shader
	//	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//	//create normal matrix
	//	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//	//send normal matrix data to shader
	//	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	//	myModelPietre.Draw(myCustomShader);
	//}


	////CORT
	//for (int k = 0; k < 3; k++) {
	//	for (int i = 0; i < 3; i++) {
	//		model = glm::mat4(1.0f);
	//		model = glm::translate(model, glm::vec3(10.0f - 2 * i, -0.2f, -3.0f - 2 * k));
	//		model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	//		model = glm::scale(model, glm::vec3(0.08f, 0.08f, 0.08f));
	//		//send model matrix data to shader
	//		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//		//create normal matrix
	//		normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//		//send normal matrix data to shader
	//		glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	//		myModelCort.Draw(myCustomShader);
	//	}
	//}



	//Draw a white cube around the light

	lightShader.useShaderProgram();
	glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
	model = glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::translate(model, lightDir);
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	//lightCube.Draw(lightShader);
}

int main(int argc, const char * argv[]) {

	initOpenGLWindow();
	initOpenGLState();
	initFBOs();
	initModels();
	initShaders();
	initUniforms();
	glCheckError();
	while (!glfwWindowShouldClose(glWindow)) {
		renderScene();

		glfwPollEvents();
		glfwSwapBuffers(glWindow);
	}

	//close GL context and any other GLFW resources
	glfwTerminate();

	return 0;
}
