#version 410 core

in vec3 normal;
in vec4 fragPosEye;
in vec4 fragPos;
in vec4 fragPosLightSpace;
in vec2 fragTexCoords;

out vec4 fColor;

uniform	mat3 normalMatrix;
uniform mat3 lightDirMatrix;
uniform	vec3 lightColor;
uniform	vec3 lightDir;
uniform vec3 posOfPointLight[30];

uniform sampler2D diffuseTexture;
uniform sampler2D specularTexture;
uniform sampler2D shadowMap;

uniform bool turnOnOffLight;
uniform bool turnOnOffPointLight;
uniform bool turnOnOffFog;


vec3 ambient;
float ambientStrength = 0.8f;

vec3 diffuse;
vec3 specular;
float specularStrength = 1.0f;
float shininess = 1024.0f;

void computeLightComponents()
{		
	vec3 cameraPosEye = vec3(0.0f);//in eye coordinates, the viewer is situated at the origin
	
	//transform normal
	vec3 normalEye = normalize(normalMatrix * normal);	
	
	//compute light direction
	vec3 lightDirN = normalize(lightDirMatrix * lightDir);	

	//compute view direction 
	vec3 viewDirN = normalize(cameraPosEye - fragPosEye.xyz);
	
	//compute half vector
	vec3 halfVector = normalize(lightDirN + viewDirN);
		
	//compute ambient light
	ambient = ambientStrength * lightColor;
	
	//compute diffuse light
	diffuse = max(dot(normalEye, lightDirN), 0.0f) * lightColor;
	
	//compute specular light
	float specCoeff = pow(max(dot(halfVector, normalEye), 0.0f), shininess);
	specular = specularStrength * specCoeff * lightColor;
}

float constant = 1.0f;
float linear = 0.00014f;
float quadratic = 1.0f;

vec3 computePointLightComponents(vec3 pointLightPosition)
{
    vec3 cameraPosEye = vec3(0.0f);
	//compute distance to light
	float dist = length(pointLightPosition - fragPos.xyz);
	//compute attenuation
	float att = 1.0f / (constant + linear * dist + quadratic * (dist * dist));
	//transform normal
	vec3 normalEye = normalize(normalMatrix * normal);	
	//compute light direction
	vec3 lightDirN = normalize(pointLightPosition - fragPos.xyz);	
	//compute view direction 
	vec3 viewDirN = normalize(pointLightPosition - fragPos.xyz);
	//compute ambient light
	ambient = ambientStrength * lightColor;
	
	//compute diffuse light
	diffuse = max(dot(normalEye, lightDirN), 0.0f) * lightColor;
	
	//compute specular light
	vec3 reflection = reflect(-lightDirN, normalEye);
	float specCoeff = pow(max(dot(viewDirN, reflection), 0.0f), shininess);
	specular = specularStrength * specCoeff * lightColor;
	
	//compute ambient light
	ambient = att * ambientStrength * lightColor;                         
	//compute diffuse light
	diffuse = att * max(dot(normalEye, lightDirN), 0.0f) * lightColor;
	specular = att * specularStrength * specCoeff * lightColor;
	
	
	//modulate with diffuse map
	ambient *= vec3(texture(diffuseTexture, fragTexCoords));
	diffuse *= vec3(texture(diffuseTexture, fragTexCoords));
	//modulate woth specular map
	specular *= vec3(texture(specularTexture, fragTexCoords));
	return ambient + diffuse + specular;
		
}

float computeShadow()
{	
	// perform perspective divide
    vec3 normalizedCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    if(normalizedCoords.z > 1.0f)
        return 0.0f;
    // Transform to [0,1] range
    normalizedCoords = normalizedCoords * 0.5f + 0.5f;

    // Get closest depth value from light's perspective
    float closestDepth = texture(shadowMap, normalizedCoords.xy).r;    

    // Get depth of current fragment from light's perspective
    float currentDepth = normalizedCoords.z;

    // Check whether current frag pos is in shadow
  //  float bias = 0.005f;
    float bias = max(0.05f * (1.0f - dot(normal, lightDir)), 0.005f);
	float shadow = currentDepth - bias> closestDepth  ? 1.0f : 0.0f;

    return shadow;	
}

float computeFog(float fogDensity){
    float fragmentDistance = length(fragPosEye);
	float fogFactor = exp(-pow(fragmentDistance * fogDensity, 2));

	 return clamp(fogFactor, 0.0f, 1.0f);
}

void main() 
{
    vec3 finalColor = vec3(0.0f, 0.0f, 0.0f);
	vec3 dirLightColor = vec3(0.0f, 0.0f, 0.0f);
	vec3 pointLightColor = vec3(0.0f, 0.0f, 0.0f);

     if (turnOnOffLight) 
	 {
		computeLightComponents();
		
		float shadow = computeShadow();
		
		//modulate with diffuse map
		ambient *= vec3(texture(diffuseTexture, fragTexCoords));
		diffuse *= vec3(texture(diffuseTexture, fragTexCoords));
		//modulate woth specular map
		specular *= vec3(texture(specularTexture, fragTexCoords));
		
		//modulate with shadow
		vec3 color = min((ambient + (1.0f - shadow) * diffuse) + (1.0f - shadow) * specular, 1.0f);
		dirLightColor = color;
	}

	if (turnOnOffPointLight)
	{
		for(int i = 0; i < 22; i++)
		pointLightColor += computePointLightComponents(posOfPointLight[i]);
	}	
	
	if(turnOnOffLight || turnOnOffPointLight){
		finalColor = dirLightColor + pointLightColor;
	}

	if(turnOnOffFog){
		float fogFactor = computeFog(0.08f);
		vec4 fogColor = vec4(0.5f, 0.5f, 0.5f, 1.0f);
		fColor = fogColor*(1-fogFactor) + vec4(finalColor, 1.0f)*fogFactor;
	}else{
		fColor = vec4(finalColor, 1.0f);
	}
}

